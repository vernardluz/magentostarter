After cloning this repository, run:

	vagrant up

This should load the Scotch Box Pro vagrant box.

However, to install Magento sample data, I have prepared a provision script which will handle the installation without you having to do the installation yourself.

This might take some time as the Magento installation with sample is large, but once it is done, you should be able to access your Magento store at http://192.168.33.10
.
To run the provision script, SSH into your vagrant box by running:

	vagrant ssh

After that, run this command:

	wget -O /var/www/script.sh https://www.dropbox.com/s/u280elcr3t0biha/script.sh?dl=0
	
And then execute that bash script by running this command:

	/bin/bash /var/www/script.sh
